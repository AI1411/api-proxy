FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="akira ishii"

COPY ./default.conf /etc/nginx/default.conf

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

RUN mkdir -p /work/backend
RUN chmod 755 /work/backend
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf
